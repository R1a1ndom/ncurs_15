/*

 NCURS_ETC visual test

 */

#include <unistd.h>
#include <curses.h>

#include "ncurs_etc.h"

int main()
{
  const int frame_height = 15;
  const int frame_width  = 25;

  scr_point frame_start,frame_max;
  WINDOW *test_win;

  initscr();
  noecho();

  getmaxyx(stdscr,frame_max.row,frame_max.col);               // try to draw frame aroung STDSCR
  draw_frame(frame_max.row, frame_max.col, zero_point, show_frame);

  frame_start = get_middle();

  frame_start.row -= (frame_height / 2);
  frame_start.col -= (frame_width  / 2);

  move(5,5);
  attron(A_DIM);
  addstr("NCURS_ETC unit test");
  draw_frame(frame_height, frame_width, frame_start, show_frame); // show frame...
  refresh();
  getch();

  draw_frame(frame_height, frame_width, frame_start, hide_frame); // hide frame...
  refresh();
  sleep(2);

  draw_frame(frame_height, frame_width, frame_start, show_frame); // show frame...
  refresh();
  sleep(2);

  attron(A_ITALIC);
  draw_frame(frame_height, frame_width, frame_start, show_frame); // and show bold frame !
  refresh();
  sleep(2);

  test_win = newwin(frame_height, frame_width, frame_start.row, frame_start.col);
  wrefresh(test_win);
  sleep(2);

  wdraw_frame(test_win,frame_height, frame_width, zero_point, show_frame); // and show bold frame !
  wrefresh(test_win);
  getch();

  endwin();
  return 0;
}
