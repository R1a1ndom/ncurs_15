/*
  --- TEST_MIX.C ---

  NCURS_15 testing program
  mixing tests

 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "base.h"
#include "gen_mix.h"
#include "con_debug_print.h"

enum {
  max_iteration = 20,
};

int main()
{
  line_arr test_arr;
  int test_low, test_up, count;

  srand(time(NULL));
  line_arr_init(test_arr, max_num);

  print_line_arr(test_arr);
  for (count = 0; count < max_iteration; count++) {
    get_two_nums(max_num,&test_low,&test_up);
    print_mix_nums(test_low,test_up);
  }

  return 0;
}