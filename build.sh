#!/bin/bash +x

# LAZYCODE build script

source libbuild.shlib

# constants for CASE

act_debug_build=1
act_release_build=2
act_mix_test=3
act_ncurs_etc_test=4
act_clear_dirs=0

src_filelist=( "ncurs_etc" "main_menu" "title_scr" "game_menu" "game_play" "main" )

deleted_obj_list=( "*.o" "*~" "ncurs_15" )
test_filenames=( "test_mix" "test_ncurs_etc" )

obj_files_num=0  #  obj files counter - global variable
obj_files_all=6  #  obj files all quantity

write_choice_prompt()
{
  echo -e "\e[1;34m---------------------------------------------"
  echo -e "\e[35mNCURS_15 simple puzzle game"
  echo -e "\e[36m  ---  build script ---"
  echo -e "\e[0;32mChoose an action:\e[0m"
  echo -e "\e[1;34m---------------------------------------------\e[0m"
  echo -e "\e[1;33m--- --- APPLICATION BUILD --- ---"
  echo -e "\e[1;33m[ 1 ]\e[0m build DEBUG version"
  echo -e "\e[1;33m[ 2 ]\e[0m build RELEASE version"
  echo -e "\e[1;33m  --- --- TESTS --- ---"
  echo -e "\e[1;33m[ 3 ]\e[0m MIXING test"
  echo -e "\e[1;33m[ 4 ]\e[0m NCURS_ETC visual test"
  echo -e "\e[1;33m  --- --- OTHER --- ---"
  echo -e "\e[1;33m[ 0 ]\e[0m CLEAR directories from temporary files"
  echo -e "\e[1;33m[ any key ]\e[0m to EXIT"
  echo -ne "\e[1;34m---------------------------------------------\e[0m"
}

gcc_debug_filelist() # $1 - output_dir
{
  for src_file in ${src_filelist[*]}
  do
    echo -en "Compiling file \e[33m$src_file\e[0m... "
    gcc -c -g -Wall -D DEBUG "src/$src_file.c" -o "bin/debug/$src_file.o"
    if [ $? -eq 0 ]; then
      echo -e "\e[32mOK!\e[0m"
      obj_files_num=$(( $obj_files_num + 1 ))
    else
      echo -e "\e[31mnot OK...\e[0m"
      sleep 1
      exit 1
    fi
  done
}

gcc_release_filelist() # $1 - output_dir
{
  for src_file in ${src_filelist[*]}
  do
    echo -en "Compiling file \e[33m$src_file\e[0m... "
    gcc -c -Wall "src/$src_file.c" -o "bin/release/$src_file.o"
    if [ $? -eq 0 ]; then
      echo -e "\e[32mOK!\e[0m"
      obj_files_num=$(( $obj_files_num + 1 ))
    else
      echo -e "\e[31mnot OK...\e[0m"
      sleep 1
      exit 1
    fi
  done
}

purge_dir()
{
  for deleted_obj in ${deleted_obj_list[*]}
  do
    rm $1/$deleted_obj 2> /dev/null
  done
}

# --- --- --- --- --- ---
# ---   MAIN SCRIPT   ---
# --- --- --- --- --- ---

write_choice_prompt
read -n 1 action
case $action in

# BUILDING DEBUG VERSION

  $act_debug_build)
    create_bin_dirs
    echo -e "\e[1;34mBuilding DEBUG version:\e[0m"
    gcc_debug_filelist
    if [ $obj_files_num -eq $obj_files_all ]; then
      echo -e "\e[1;34mLinking ...\e[0m"
      cd $dir_bin_debug
      gcc -g -o "ncurs_15" $(ls *.o) -lncurses
      if [ $? -eq 0 ]; then
        print_result
        echo "DEBUG version is READY!"
      else
        print_result
        echo "BUILDING failure..."
      fi
    else
      echo -e "\e[1;34mNothing to linking.\e[0m"
    fi
    sleep 1
  ;;

# BUILDING RELEASE VERSION

  $act_release_build)
    create_bin_dirs
    echo -e "\e[1;34mBuilding RELEASE version:\e[0m"
    gcc_release_filelist
    if [ $obj_files_num -eq $obj_files_all ]; then
      echo -e "\e[1;34mLinking ...\e[0m"
      cd $dir_bin_release
      gcc -O3 -o "ncurs_15" $(ls *.o) -lncurses
      if [ $? -eq 0 ]; then
        print_result
        echo "RELEASE version is READY!"
      else
        print_result
        echo "BUILDING failure..."
      fi
    else
      echo -e "\e[1;34mNothing to linking.\e[0m"
    fi
    sleep 1

  ;;

# MIXING TEST

  $act_mix_test)
    echo -e "\n\e[35mGEN_MIX_TEST\e[0m build and run..."

    gcc -g -c -Wall -D FOR_TEST -o "tests/gen_mix.o" "src/gen_mix.c"
    gcc -g -c -Wall -o "tests/con_debug_print.o" "src/con_debug_print.c"
    gcc -Isrc -g -c -Wall -o "tests/test_mix.o" "tests/test_mix.c"

    if [ -f "tests/gen_mix.o" ] && [ -f "tests/con_debug_print.o" ]; then
      gcc -Isrc -g -o "tests/test_mix" $(ls tests/*.o) -lncurses
      if [ $? -eq 0 ];then
        echo -e "\e[1;32mSuccessfully !!!\e[0m"
        sleep 1
        ./tests/test_mix
      else
        echo -e "\e[1;31mTest not builded...\e[0m"
      fi
    else
      echo -e "\e[1;31Nothing to linking.\e[0m"
      sleep 1
      exit 1
    fi
    rm tests/*.o 2> /dev/null
    rm tests/test_mix  2> /dev/null
  ;;

# NCURS_ETC test

  $act_ncurs_etc_test)
    echo -e "\n\e[35mNCURS_ETC_TEST\e[0m build and run..."
    gcc -g -c -Wall -o "tests/ncurs_etc.o" "src/ncurs_etc.c"
    gcc -Isrc -g -c -Wall -o "tests/test_ncurs_etc.o" "tests/test_ncurs_etc.c"
    if [ -f "tests/ncurs_etc.o" ] && [ -f "tests/test_ncurs_etc.o" ]; then
      gcc -Isrc -g -o "tests/test_ncurs_etc" $(ls tests/*.o) -lncurses
      if [ $? -eq 0 ];then
        echo -e "\e[1;32mSuccessfully !!!\e[0m"
        sleep 1
        ./tests/test_ncurs_etc
      else
        echo -e "\e[1;31mTest not builded...\e[0m"
      fi
    else
      echo -e "\e[1;31Nothing to linking.\e[0m"
      sleep 1
      exit 1
    fi
    rm tests/*.o 2> /dev/null
    rm tests/test_ncurs_etc  2> /dev/null
  ;;

# CLEAR DIRECTORIES from compile results

  $act_clear_dirs)
    echo -e "\nFollowing files will br deleted:\e[1;33m"
    echo $(ls -1 $dir_src/*.o $dir_src/*~ $dir_src/test_mix 2>/dev/null)
    echo $(ls -1 $dir_bin_debug/*.o $dir_bin_debug/ncurs_15 2>/dev/null)
    echo $(ls -1 $dir_bin_release/*.o $dir_bin_release/ncurs_15 2>/dev/null)
    purge_dir $dir_root
    purge_dir $dir_src
    purge_dir $dir_bin_debug
    purge_dir $dir_bin_release
    echo -e "\e[1;34mR E A D Y !\e[0m"
  ;;

  *)
    echo "Exit from script."
  ;;
esac