/*

  --- GAME_PLAY.C ---
  part of NCURS_15 puzzle game

 */

#include <curses.h>
#include <unistd.h>
#include <string.h>

#include "ncurs_etc.h"
#include "base.h"

#include "game_play.h"

#include "game_menu.h"

#ifdef DEBUG
void debug_print_gameplay_elements(game_play_elements_pos *elements)
{
  int i = 0;
  scr_point current_max;
  getmaxyx(stdscr, current_max.row, current_max.col);
  mvwprintw(stdscr,i,0,"debug print:");
  i++;
  mvwprintw(stdscr, i, 0, "max:   [%d %d]",
            current_max.row, current_max.col);
  i++;
  mvwprintw(stdscr, i, 0, "field: [%d %d]",
            elements->field_pos.row, elements->field_pos.col);
  i++;
  mvwprintw(stdscr, i, 0, "menu:  [%d %d]",
            elements->game_menu_pos.row, elements->game_menu_pos.col);
  i++;
  mvwprintw(stdscr, i, 0, "winner:[%d %d]",
            elements->winner_win_pos.row, elements->winner_win_pos.col);
}
#endif

game_play_elements_pos get_game_play_elements_pos()
{
  game_play_elements_pos tmp_pos;
  scr_point mid_pos;

  mid_pos = get_middle();

  tmp_pos.field_pos.row = mid_pos.row - (field_height / 2);
  tmp_pos.field_pos.col = mid_pos.col - (field_width / 2);

  tmp_pos.game_menu_pos.row = mid_pos.row - (game_menu_win_height / 2);
  tmp_pos.game_menu_pos.col = mid_pos.col - (game_menu_win_width / 2);

  tmp_pos.winner_win_pos.row = mid_pos.row - (winner_win_height / 2);
  tmp_pos.winner_win_pos.col = mid_pos.col - (winner_win_width / 2);

  return tmp_pos;
}

/* --- *** --- */

void draw_grid(WINDOW *field_win)
{
  int field_col,field_row;

  for(field_row = 0; field_row <= field_height; field_row += (cell_height+1))
  {
    for(field_col = 0; field_col <= field_width; field_col += (cell_width+1))
      mvwaddch(field_win,field_row,field_col,'+');
  }
}

/* --- *** --- */

void redraw_game_field(WINDOW *field_win, game_play_elements_pos *elements)
{
#ifdef DEBUG
  debug_print_gameplay_elements(elements);
#endif
  wclear(field_win);
  draw_grid(field_win);
}

/* --- *** --- */


/* --- GAME_PLAY --- */

void game_play(program_state *state)
{
  game_play_elements_pos elements;
  WINDOW *field_win;
  chtype sym;

  clear();

  elements = get_game_play_elements_pos();
  field_win = newwin(field_height,
                     field_width,
                     elements.field_pos.row,
                     elements.field_pos.col);
  redraw_game_field(field_win, &elements);
  wrefresh(stdscr);
  wrefresh(field_win);
  do {
    sym = getch();
    switch (sym) {
      case local_esc_key:
        *state = game_menu(&elements, field_win, menu_list);
        break;

      case KEY_RESIZE:
        clear();
        elements = get_game_play_elements_pos();
        mvwin(field_win,
              elements.field_pos.row,
              elements.field_pos.col);
        *state = st_continue;
        break;

      default:
        *state = st_continue;

    }
    if (*state==st_continue) {
      redraw_game_field(field_win, &elements);
      wrefresh(stdscr);
      wrefresh(field_win);
    }
  } while (*state != st_quit_to_main_menu); // DO
  delwin(field_win);
}

/* --- *** --- */
