/*
  --- DEBUG_PRINT.H ---
 part of ncurs15 puzzle game
 debug prints for test programs

*/

#ifndef C_15_CON_DEBUG_PRINT_H
#define C_15_CON_DEBUG_PRINT_H

void print_line_arr(line_arr arr);

void print_mix_nums(int, int);

#endif