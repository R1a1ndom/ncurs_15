/*
  --- DEBUG_PRINT.C ---

  Part of NCURS_15 puzzle game
  console OBdebug print for test programs

*/

#include <stdio.h>
#include "base.h"

#include "con_debug_print.h"

void print_line_arr(line_arr arr)
{
  int i=0;
  while(i<max_num)
  {
    printf("[ %2d ] ",arr[i]);
    if ((arr[i]%5) == 0 )
    {
      printf("\n");
    }
    i++;
  }
  return;
}

void print_mix_nums(int low_num, int up_num)
{
  printf("\n< %d > < %d >\n",low_num,up_num);
  return;
}