/*
  --- GAME_PLAY.H ---
  part of CNURS_15 puzzle game

 */

#ifndef GAME_PLAY_H
#define GAME_PLAY_H


enum {
  cell_width = 13,    // 2*digit_width + 1
  cell_height = 5,    // digit_height
  field_width = 57,   // 4*cell_width  + 5
  field_height = 26,  // 4*cell_height + 5

  winner_win_height   = 15,
  winner_win_width    = 40,
};

typedef struct game_play_elements_pos {
  scr_point field_pos;
  scr_point game_menu_pos;
  scr_point winner_win_pos;
} game_play_elements_pos;

game_play_elements_pos get_game_play_elements_pos();

#ifdef DEBUG
void debug_print_gameplay_elements(game_play_elements_pos *elements);
#endif

void draw_grid(WINDOW *field_win);

void redraw_game_field(WINDOW *field_win, game_play_elements_pos *elements);

void game_play(program_state *state);

#endif