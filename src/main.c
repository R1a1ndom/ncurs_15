/*

 --- MAIN.C ---

 NCURS_15 simple puzzle game
 main file

 */

#include <curses.h>
#include <unistd.h>

#include "base.h"
#include "ncurs_etc.h"
#include "title_scr.h"
#include "game_play.h"

int main()
{
  program_state game_state = st_init;
  int menu_pos = 0;

  initscr();
  crmode();
  noecho();
#ifndef DEBUG
  curs_set(0);
#endif
  do {
    title_screen(&game_state,&menu_pos);
    if (game_state == st_begin_game)
    {
      menu_pos = 0;
      game_play(&game_state);
    }
  } while (game_state != st_quit_to_shell);
  endwin();
  return 0;
}