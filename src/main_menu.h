/*

 MAIN_MENU.H - part of ncurs_15 puzzle game
 drawing main menu and selecting menu items

*/

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

extern const char *main_menu_list[];

enum {
  main_menu_win_height  = 11,
  main_menu_win_width   = 24,

  main_menu_item_height =  3,
  main_menu_item_width  = 20,

  main_menu_max_pos     =  2,
};



void write_full_menu
  (WINDOW *menu_win, const char **menu_list, int menu_max, int menu_current_pos);

#endif