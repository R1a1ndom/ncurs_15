/*
  --- GEN_MIX.H ---
  part of NCURS_15 puzzle game
  generating and mixing array 1-15

*/

#ifndef C_15_GEN_MIX_H
#define C_15_GEN_MIX_H

void line_arr_init(line_arr arr, int arr_len);

int get_rand_number(int);

void get_two_nums(int max, int* low_num, int* high_num);

#endif