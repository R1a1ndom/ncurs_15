/*
  --- GEN_MIX.C ---
  part of NCURS_15 puzzle game
  generating and mixing array of numbers 1 - 15

*/

#include <stdio.h>
#include <stdlib.h>

#include "base.h"
#include "gen_mix.h"

// init line_arr[] array

void line_arr_init(line_arr arr, int arr_len)
{
  int i;
  for(i = 0; (arr[i] = i+1) < arr_len;i++)
    {}
  return;
}

// get random number 0..max

int get_rand_number(int max)
{
  return 1 + (int)((double)max_num*rand()/(RAND_MAX+1.0));
}

// getting 2 random numbers 2..14

void get_two_nums(int max, int* low_num, int* high_num)
{
  int tmp_low = 0,tmp_high = 0;
  int tmp_tmp;

  tmp_low  = get_rand_number(max_num - 3);
#ifdef FOR_TEST
  printf("l=%d",tmp_low);
#endif
  do {
    tmp_high = get_rand_number(max_num - 3);
#ifdef FOR_TEST
    printf(" h=%d ",tmp_high);
#endif
  } while ( (tmp_high == tmp_low) ||
            (abs(tmp_high - tmp_low) == 1) ); // DO

  if (tmp_low > tmp_high) {
    tmp_tmp  = tmp_low;
    tmp_low  = tmp_high;
    tmp_high = tmp_tmp;
  }

  *low_num  = tmp_low;
  *high_num = tmp_high;
  return;
}