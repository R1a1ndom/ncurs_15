/*
 * GAME_MENU.C - part of NCURS_15 puzzle game
 * game menu
 *
 */

#ifndef NCURS_15_GAME_MENU
#define NCURS_15_GAME_MENU

enum {
  game_menu_win_height =  9,
  game_menu_win_width  = 43,

  game_menu_item_height =  3,
  game_menu_item_width  = 13,

  game_menu_max = 2,

  game_menu_item_len = 12,
  game_menu_note_len = 22,
};

typedef struct game_menu_item {
  char item[game_menu_item_len];
  char note[game_menu_note_len];
} game_menu_item;


extern const game_menu_item menu_list[];

/* ******* */

void draw_game_menu_item(WINDOW *game_menu_win,
                         scr_point start_point,
                         game_menu_item menu_item,
                         menu_state game_menu_state);

void write_full_game_menu(WINDOW *menu_win,
                          const game_menu_item *menu_list,
                          int game_menu_current_pos,
                          int game_menu_max);

program_state game_menu(game_play_elements_pos *elements,
                        WINDOW *field_win,
                        const game_menu_item *menu_list);

#endif