/*

  TITLE_SCR.C - part of ncurs_15 puzzle game

  title screen functions and constants

 */

#include <unistd.h>
#include <string.h>
#include <curses.h>

#include "base.h"
#include "ncurs_etc.h"
#include "title_scr.h"
#include "main_menu.h"

static const char *game_title[] = {
  "NNNNN   CCCC  UU  UU  RR RR   SSSS          11 555555",
  "NN  NN CC  CC UU  UU  RRR RR SS           1111 55    ",
  "NN  NN CC     UU  UU  RR      SSS    ===    11 555555",
  "NN  NN CC  CC UU  UU  RR        SS   ===    11     55",
  "NN  NN  CCCC   UUU UU RR     SSSS           11 555555",
  NULL
};


scr_point description_p = {4,5};
scr_point about_picture_p = {11,7};

static const char *about_title[] = {
  "-> About NCURS_15 application <-",
  "[ Press ANY KEY to continue... ]",
};

static const char *app_description[] = {
  "Simple console puzzle game",
  "*NIX application using NCURSES library",
  "(c) Alexey Sorokin , 2019",
  "",
  "creator of original game:",
  "       Noyes Palmer Chapman, USA, 1878",
    NULL
};

static const char *about_picture[] = {
" 5   1   2   3        1   2   3   4",
"",
" 9   6   7   4   ->   5   6   7   8",
"                 ->",
"13  10  12   8   ->   9  10  11  12",
"",
"    14  15  11       13  14  15",
NULL
};

#ifdef DEBUG
void debug_print_elements(const title_elements_pos *elements)
{
  int i = 0;
  scr_point current_max;
  getmaxyx(stdscr,current_max.row,current_max.col);
  mvprintw(i, 0, "debug:");
  i++;
  mvprintw(i, 0, "max:  [%d %d]",
           current_max.row, current_max.col);
  i++;
  mvprintw(i, 0, "title:[%d %d]",
           elements->title_pos.row, elements->title_pos.col);
  i++;
  mvprintw(i, 0, "menu: [%d %d]",
           elements->main_menu_pos.row, elements->main_menu_pos.col);
  i++;
  mvprintw(i, 0, "about:[%d %d]",
           elements->about_win_pos.row, elements->about_win_pos.col);
}
#endif

// *** *** ***

title_elements_pos get_title_scr_elements_pos()
{
  title_elements_pos tmp;
  scr_point mid_point;

  mid_point = get_middle();

  tmp.title_pos.row = mid_point.row - title_height - 1;
  tmp.title_pos.col = mid_point.col - strlen(game_title[0]) / 2;

  tmp.main_menu_pos.row = mid_point.row;
  tmp.main_menu_pos.col = mid_point.col - (main_menu_width / 2);

  tmp.about_win_pos.row = mid_point.row - (about_win_height / 2);
  tmp.about_win_pos.col = mid_point.col - (about_win_width  / 2);

  return tmp;
}


// *** *** ***

void redraw_title_screen
     (title_elements_pos *elements, WINDOW *main_menu_win, int menu_pos)
{
#ifdef DEBUG
  debug_print_elements(elements);
#endif
  attron(A_BOLD);
  add_string_arr(elements->title_pos,game_title);
  attroff(A_BOLD);
  mvwin(main_menu_win,
        elements->main_menu_pos.row,
        elements->main_menu_pos.col);
  write_full_menu(main_menu_win,
                  main_menu_list,
                  main_menu_max_pos,
                  menu_pos);
  wrefresh(stdscr);
  wrefresh(main_menu_win);
}

// *** *** ***

void draw_about_window(WINDOW *about_win)
{
  wdraw_frame(about_win,
              about_win_height,
              about_win_width,
              zero_point,
              show_frame);

  mvwprintw(about_win,2,
           (about_win_width - strlen(about_title[0])) / 2,
            "%s",about_title[0]);

  wattron(about_win,A_BOLD);
  wadd_string_arr(about_win,description_p,app_description);
  wattroff(about_win,A_BOLD);
  wadd_string_arr(about_win,about_picture_p,about_picture);

  wattron(about_win,A_REVERSE);
  mvwprintw(about_win,
            about_win_height - 3,
           (about_win_width - strlen(about_title[1])) / 2,
            "%s",about_title[1]);
  wattroff(about_win,A_REVERSE);
}

// *** *** ***

void show_about_window(title_elements_pos *elements,
                       WINDOW *main_menu_win,
                       int menu_pos)
{
  WINDOW *show_about_win;
  chtype sym;
  program_state about_state = st_init;

  show_about_win = newwin(about_win_height,
                          about_win_width,
                          elements->about_win_pos.row,
                          elements->about_win_pos.col);
  draw_about_window(show_about_win);
  keypad(show_about_win,1);
  wrefresh(show_about_win);
  do {
    sym = wgetch(show_about_win);
    if (sym == KEY_RESIZE)
    {
      clear();
      *elements = get_title_scr_elements_pos();
      redraw_title_screen(elements, main_menu_win, menu_pos);
      mvwin(show_about_win,
            elements->about_win_pos.row,
            elements->about_win_pos.col);
      wrefresh(show_about_win);
    } else {
      about_state = st_continue;
    }
  } while (about_state != st_continue); // DO-WHILE
  delwin(show_about_win);
  clear();
  redraw_title_screen(elements, main_menu_win, menu_pos);
}

// *** *** ***

void title_screen(program_state *old_state, int *menu_pos)
{
  chtype sym;
  WINDOW *main_menu_win;
  title_elements_pos elements;

  clear();

  elements = get_title_scr_elements_pos();      //  drawing on STDSCR window
  main_menu_win = newwin(main_menu_win_height,  //  drawing on MAIN_MENU_WIN window
                         main_menu_win_width,
                         elements.main_menu_pos.row,
                         elements.main_menu_pos.col);
  redraw_title_screen(&elements, main_menu_win, *menu_pos);
  keypad(main_menu_win,TRUE);   //  keyboard reading
  while (((sym = wgetch(main_menu_win)) != local_esc_key)
       && (sym != local_enter_key))
  {
    switch(sym)
    {
      case KEY_DOWN:
        if ((*menu_pos) < main_menu_max_pos)
          (*menu_pos)++;
        break;
      case KEY_UP:
        if ((*menu_pos) > 0)
          (*menu_pos)--;
        break;
      case KEY_RESIZE:
        clear();
        elements = get_title_scr_elements_pos();
        break;
    }
    redraw_title_screen(&elements, main_menu_win, *menu_pos);
  }
  if (sym == local_esc_key)     // pressed keys handling
    *old_state = st_quit_to_shell;
  else if (sym == local_enter_key)
    {
      if ((program_state) (*menu_pos) == st_show_about)
        show_about_window(&elements,
                          main_menu_win,
                          *menu_pos);
      else
        *old_state = (program_state) *menu_pos;
    }
  else
    *old_state = st_continue;

  delwin(main_menu_win);

  return;
}

