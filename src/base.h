/*
  --- BASE.H ---

 part of C_15 puzzle game
 base types and constants

 */

#ifndef C_15_BASE_H
#define C_15_BASE_H

/* ESC and ENTER keys  */

enum {
  local_esc_key   =   27,
  local_enter_key = '\n',  // in WINDOWS / DOS enter key = 13
};                       // in UNIX / Linux  enter key = 10
                         // :-)
enum {
  max_num     = 15,
  field_size  =  3,
};

typedef enum program_state{
  st_begin_game    = 0,
  st_show_about,
  st_quit_to_shell,

  st_restart,
  st_begin_new,
  st_quit_to_main_menu,

  st_init,
  st_continue,
} program_state;


typedef enum menu_state {
  menu_empty,
  menu_selected
} menu_state;


typedef int line_arr[max_num];

typedef int game_field[field_size][field_size];

#endif