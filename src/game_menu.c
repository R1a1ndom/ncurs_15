/*
 * GAME_MENU.C - part of NCURS_15 puzzle game
 * in-game menu
 *
 */

#include <curses.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ncurs_etc.h"
#include "base.h"
#include "game_play.h"
#include "game_menu.h"

/* --- *** --- */

const char game_menu_title[] =
"NCURS_15 in-game MENU";

const game_menu_item menu_list[] = {
  { "  RESTART  ", "Restart current game" },
  { " NEW  GAME ", "Begin new game      " },
  { "    EXIT   ", "Exit to main menu   " },
};

const scr_point game_menu_draw_start = { 3, 2 };

/* --- *** --- */

void draw_game_menu_static_elements(WINDOW *game_menu_win)
{
  wdraw_frame(game_menu_win,
              game_menu_win_height,
              game_menu_win_width,
              zero_point,
              show_frame);
  mvwprintw(game_menu_win,
            1, (game_menu_win_width - strlen(game_menu_title)) / 2,
            "%s", game_menu_title);
}

/* --- *** --- */

void draw_game_menu_item(WINDOW *game_menu_win,
                         scr_point start_point,
                         game_menu_item menu_item,
                         menu_state game_menu_state)
{
  if (game_menu_state == menu_selected) {
    wattrset(game_menu_win,A_NORMAL);
    mvwprintw(game_menu_win,
              game_menu_win_height-2,1,
              "%s",menu_item.note);
    wattrset(game_menu_win,A_BOLD);
    wdraw_frame(game_menu_win,
                game_menu_item_height,
                game_menu_item_width,
                start_point,
                show_frame);
  } else {
    wattrset(game_menu_win,A_DIM);
  }
    mvwprintw(game_menu_win,
              start_point.row+1,start_point.col+1,
              "%s",menu_item.item);
    wattrset(game_menu_win,A_NORMAL);
}

/* --- *** --- */

void write_full_game_menu(WINDOW *menu_win,
                          const game_menu_item *menu_list,
                          int game_menu_current_pos,
                          int game_menu_max)
{
  scr_point draw_point;
  int i = 0;

  draw_point = game_menu_draw_start;

  wattrset(menu_win,A_NORMAL);
  wclear_rect(menu_win,
              draw_point,
              game_menu_item_height,
              game_menu_item_width*3);

  while(i < game_menu_current_pos)
  {
     draw_game_menu_item(menu_win,
                         draw_point,
                         menu_list[i],
                         menu_empty);
     draw_point.col += game_menu_item_width;
     i++;
  }

  draw_game_menu_item(menu_win,
                      draw_point,
                      menu_list[i],
                      menu_selected);
#ifdef DEBUG
  mvwprintw(menu_win,1,1,"%d",i);
#endif
  draw_point.col += game_menu_item_width;
  i++;

  while(i <= game_menu_max) {
     draw_game_menu_item(menu_win,
                         draw_point,
                         menu_list[i],
                         menu_empty);
     draw_point.col += game_menu_item_width;
     i++;
  }
  wattron(menu_win,A_STANDOUT);
  return;
}

/* --- *** --- */

program_state game_menu(game_play_elements_pos *elements,
                        WINDOW *field_win,
                        const game_menu_item menu_list[])
{
  WINDOW *game_menu_win;
  chtype sym;
  int game_menu_pos = 0;
  int need_to_refresh = 0;

  game_menu_win = newwin(game_menu_win_height,
                         game_menu_win_width,
                         elements->game_menu_pos.row,
                         elements->game_menu_pos.col);
  draw_game_menu_static_elements(game_menu_win);
  keypad(game_menu_win,1);
  do {
    write_full_game_menu(game_menu_win,
                         menu_list,
                         game_menu_pos,
                         game_menu_max);
    if (need_to_refresh == 1) {
      wrefresh(game_menu_win);
      need_to_refresh = 0;
    }
    sym = wgetch(game_menu_win);
    switch(sym) {

      case KEY_LEFT:
        if (game_menu_pos > 0) {
          game_menu_pos--;
          need_to_refresh = 1;
        }
        break;

      case KEY_RIGHT:
        if (game_menu_pos < game_menu_max) {
          game_menu_pos++;
          need_to_refresh = 1;
        }
        break;

      case KEY_RESIZE:
      clear();
      *elements = get_game_play_elements_pos();
#ifdef DEBUG
      debug_print_gameplay_elements(elements);
#endif
      redraw_game_field(field_win, elements);
      mvwin(field_win,
            elements->field_pos.row,
            elements->field_pos.col);
      mvwin(game_menu_win,
            elements->game_menu_pos.row,
            elements->game_menu_pos.col);
      wrefresh(stdscr);
      wrefresh(field_win);
    }
  } while ((sym != local_esc_key)
       && (sym != local_enter_key));
  delwin(game_menu_win);
  if (sym == local_esc_key)
    return st_continue;
  else if (sym == local_enter_key)
    return st_restart + game_menu_pos;
#ifdef DEBUG
  else {
    endwin();
    printf("game_menu() function: this code mustn't be executed !!!");
    exit(1);
  }
#endif
}
