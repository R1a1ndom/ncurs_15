
#ifndef TITLE_SCR_H
#define TITLE_SCR_H

enum {
  title_height     =  5,
  title_width      = 20,
  main_menu_width  = 20,
  main_menu_count  =  3,
  about_win_height = 22,
  about_win_width  = 50,
};


typedef struct title_elements_pos {
  scr_point title_pos;
  scr_point main_menu_pos;
  scr_point about_win_pos;
} title_elements_pos;

#ifdef DEBUG

void debug_print_elements(const title_elements_pos *elements);

#endif

title_elements_pos get_title_scr_elements_pos();

void draw_title(const char **title, int height,scr_point point);


void redraw_title_screen
     (title_elements_pos *elements, WINDOW *main_menu_win, int menu_pos);

void show_about_window();

void title_screen(program_state *old_state, int *menu_pos);

#endif