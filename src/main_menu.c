/*
 * MAIN_MENU.C - part of ncurs_15 puzzle game
 * drawing main menu on title screen
 */

#include <curses.h>

#include "base.h"
#include "ncurs_etc.h"
#include "main_menu.h"

const char *main_menu_list[] = {
  "   BEGIN GAME   ",
  "      ABOUT     ",
  " EXIT FROM GAME " };

static void write_menu_item(WINDOW *menu_win, scr_point start_point,
                            const char *menu_string, menu_state selected)
{
  if (selected == menu_selected)
  {
    wattron(menu_win,A_BOLD);
    wdraw_frame(menu_win,
                main_menu_item_height,
                main_menu_item_width,
                start_point,
                star_frame);
  } else {
    wattron(menu_win,A_DIM);
  }
  mvwprintw(menu_win,
            start_point.row + 1,
            start_point.col + 2,
            "%s",menu_string);
  wattrset(menu_win,A_NORMAL);
  return;
}

void write_full_menu
  (WINDOW *menu_win, const char **menu_list, int menu_max, int menu_current_pos)
{
  scr_point draw_point;
  int i = 0;

  draw_point.row = 1;
  draw_point.col = 2;

  werase(menu_win);
  wdraw_frame(menu_win,
              main_menu_win_height,
              main_menu_win_width,
              zero_point,
              show_frame);

  while(i < menu_current_pos)
  {
     write_menu_item(menu_win,draw_point,menu_list[i],menu_empty);
     draw_point.row += main_menu_item_height;
     i++;
  }

  write_menu_item(menu_win,draw_point,menu_list[i],menu_selected);
  draw_point.row += main_menu_item_height;
  i++;

  while(i <= menu_max)
  {
     write_menu_item(menu_win,draw_point,menu_list[i],menu_empty);
     draw_point.row += main_menu_item_height;
     i++;
  }

  return;
}