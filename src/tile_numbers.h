/*

 TILE_NUMBERS.H - part of NCURS_15 puzzle game
 writing digits like a 7-segment indicator

 */

#ifndef TILE_NUMBERS_H
#define TILE_NUMBERS_H

#define DIGIT_MAX 9

typedef t_digit_pattern int;

enum {digit_height = 5,
      digit_cell_width = 3,
      digit_width = 6};

const t_digit_pattetn digit_patterns[DIGIT_MAX] = {
0xF0,0xF1,0xF2,0xF3,0xF4,
0xF5,0xF6,0xF7,0xF8,0xF9,
};

draw_digit(const scr_point pos, t_digit_pattern pattern);

#endif