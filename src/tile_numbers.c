/*

 TILE_NUMBERS.H - tile of ncurs_15 puzzle game
 writing numbers

 */

#include <curses.h>


#include "ncurs_etc.h"
#include "tile_numbers.h"


draw_digit(const scr_point pos, t_digit_pattern pattern)
{
  pos.col += digit_height;
  int i,j;
  for(i=digit_height;i>0;i--)
  {
     for(j=0;j<digit_width;j++)
     {
       move(pos.row-i,pos.col+(j*2));
       addch((pattern | 1) ? "[]" : "  ");
       pattern >>= 1;
     }
  }
}